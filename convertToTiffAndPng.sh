echo $1
dpi=300

for i in $(seq 1 8)
do
	convert -units PixelsPerInch Figure$i/Figure$i.svg -density $dpi Figure$i/Figure$i.tif
	convert Figure$i/Figure$i.tif Figure$i/Figure$i.png
done

for i in $(seq 1 5)
do
	convert -units PixelsPerInch SuppFig$i/SuppFig$i.svg -density $dpi SuppFig$i/SuppFig$i.tif
	convert SuppFig$i/SuppFig$i.tif SuppFig$i/SuppFig$i.png
done
