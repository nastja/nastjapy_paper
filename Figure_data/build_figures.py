import numpy as np
import json
import matplotlib.pyplot as plt
import pandas as pd
from copy import copy
import os
from nastja.DataHandler import DataHandler
import pickle
import json
import matplotlib.colors as mcolors
from scipy import stats
from joblib import Parallel, delayed


vmin = 0
vCenter = 1.5
vmax = 5
divnorm = mcolors.TwoSlopeNorm(vmin=vmin, vcenter=vCenter, vmax=vmax)
min_val_at_same_folder = 0
folders_to_show = [5,113,111,123]

save = True
div_norm = False
show = False

#Simus vs expe


lambda_suffix = ''
standardization_factor_suffix = lambda_suffix
with open('metric_comparison/standardization_factors%s.json' % standardization_factor_suffix,'r') as f:
    standardization_factors = json.load(f)
with open('metric_comparison/lambdas%s.json' % standardization_factor_suffix,'r') as f:
    lambdas = json.load(f)

with open('metric_comparison/simus_vs_expe_data_17_11_22.p', 'rb') as f:
    all_frames = pickle.load(f)

plt.rcParams.update({'font.size': 24})
#folders_to_show = [0,113,117,121]

N_replicates = 5
N_expe_replicates = 3
row_tuples = []
for folder in folders_to_show:
    for replicate in range(N_replicates):
        row_tuples.append((folder, replicate))
col_tuples = []

cols = [1,2,3,4]
its = [1,2,3]
for col in cols:
    for it in its:
        col_tuples.append((col,it))

to_plot = ['volumes','central_local_density', 'gaslike_dists', 'tumor_area', 'tumor_surface_orientation', 'overall_deviation_score']

for feature in to_plot:
    if feature != 'overall_deviation_score':
        mean_dist,std_dist,dist_shift = standardization_factors[feature]
        lambda_val = lambdas[feature]
    grid = np.zeros((len(row_tuples),len(col_tuples)),dtype=float)
    mean_grid = np.zeros((len(folders_to_show),len(cols)),dtype=float)
    for i, row in enumerate(row_tuples):
        folder, rep = row
        for j, column in enumerate(col_tuples):
            col, it = column
            index = np.where((all_frames[col][it]['Folder'] == folder) & (all_frames[col][it]['Replicate'] == rep))[0]
            print(np.min(all_frames[col][it][feature]))
            val_to_set = copy(all_frames[col][it][feature][index[0]])
            if feature != 'overall_deviation_score':
                val_to_set -= mean_dist
                val_to_set /= std_dist
                val_to_set += dist_shift
                val_to_set *= lambda_val
            grid[i,j] = val_to_set
            #print(np.min(all_frames[col][it]['overall_deviation_score']))
            #grid[i,j] = all_frames[col][it]['overall_deviation_score'][index[0]]            
            #print(grid[i,j])


    grid2 = grid
    for i in range(len(folders_to_show)):
        for j in range(len(cols)):
            mean_grid[i,j] = np.mean( grid2[i*N_replicates : (i+1)*N_replicates, j*N_expe_replicates : (j+1) * N_expe_replicates].flatten() )







    fig,ax = plt.subplots(figsize=(12,11))  
    min_val_at_same_folder = 0.
    if div_norm:
        im = ax.imshow(grid - min_val_at_same_folder,origin='lower',aspect='auto',norm=divnorm)
    else:
        if feature == 'overall_deviation_score':
            im = ax.imshow(grid - min_val_at_same_folder,origin='lower',aspect='auto',vmin = vmin, vmax=vmax)
        else:
            im = ax.imshow(grid - min_val_at_same_folder,origin='lower',aspect='auto',vmin = vmin, vmax=0.5*vmax)
    cb = fig.colorbar(im,ax=ax,spacing='proportional')
    #cb.set_ticks(list(range(vmax+1))+[vCenter])

    #fig.colorbar(im,ax=ax)
    #plt.clim((0,35))
    ax.set_xticklabels({})
    ax.set_yticklabels({})
    if save == True:
        filename = 'Figures/simu_vs_expe_standardized_%s.png' % feature
        plt.savefig(filename)
    plt.close()

    fig,ax = plt.subplots(figsize=(12,11))  
    #im = ax.imshow(mean_grid,origin='lower',norm=divnorm,aspect='auto')
    if div_norm:
        im = ax.imshow(mean_grid - min_val_at_same_folder,origin='lower',aspect='auto',norm=divnorm)
    else:
        if feature == 'overall_deviation_score':
            im = ax.imshow(mean_grid - min_val_at_same_folder,origin='lower',aspect='auto',vmin = vmin, vmax=vmax)
        else:
            im = ax.imshow(mean_grid - min_val_at_same_folder,origin='lower',aspect='auto',vmin = vmin, vmax=0.5*vmax)
    cb = fig.colorbar(im,ax=ax,spacing='proportional')
    #cb.set_ticks(list(range(vmax+1))+[vCenter])
    #fig.colorbar(im,ax=ax)
    #plt.clim((0,35))
    ax.set_xticklabels({})
    ax.set_yticklabels({})
    if save == True:
        filename = 'Figures/simu_vs_expe_standardized_%s_mean.png' % feature
        plt.savefig(filename)
    plt.close()


# Simu vs Simu



cases = 'test'


#all_frames[0][0]
all_frames = {}
if cases == 'test':
    with open('metric_comparison/simu_dh_test_case_comparison.p', 'rb') as f:
        all_frames = pickle.load(f)
else:
    with open('metric_comparison/simu_dh_comparison_17_11_22.p', 'rb') as f:
        all_frames = pickle.load(f)

N_replicates = 5
N = len(folders_to_show)*N_replicates
grid = np.zeros((N,N),dtype=float)

#mean_grid = 
for i in range(N):
    for j in range(N):
        row_folder = folders_to_show[i // N_replicates]
        row_replicate = i % N_replicates
        if cases == 'test':
            row_replicate += N_replicates
        col_folder = folders_to_show[j // N_replicates]
        col_replicate = j % N_replicates
        if cases == 'test':
            col_replicate += N_replicates
        frame = all_frames[row_folder][row_replicate]
        index = np.where((frame['Folder'] == col_folder)  & (frame['Replicate'] == col_replicate))[0]
        if index.size > 0:
            grid[i,j] = frame['overall_deviation_score'][index[0]]
        else:
            print('%d, %d, %d, %d' % (row_folder, row_replicate, col_folder, col_replicate))


grid_with_mean = grid.copy()
for i in range(len(folders_to_show)):
    for j in range(i+1):
        mean_val = np.mean(grid[(i*N_replicates):(i+1)*N_replicates,(j*N_replicates):(j+1)*N_replicates].flatten())
        if i != j:
            grid_with_mean[(i*N_replicates):(i+1)*N_replicates,(j*N_replicates):(j+1)*N_replicates] = mean_val
        else:
            for k in range(N_replicates):
                grid_with_mean[i*N_replicates+k:(i+1)*N_replicates,j*N_replicates + k] = mean_val
            #grid_with_mean[(i*N_replicates):(i+1)*N_replicates,(j*N_replicates):(j+1)*N_replicates] = mean_val
for i in range(grid.shape[0]):
    grid_with_mean[i,i] = grid[i,i]


fig,ax = plt.subplots(figsize=(12,11))  
print(div_norm)
if div_norm:
    im = ax.imshow(grid_with_mean - min_val_at_same_folder,origin='lower',norm=divnorm)
else:
    im = ax.imshow(grid_with_mean - min_val_at_same_folder,origin='lower',vmin = vmin, vmax=vmax)
    
cb = fig.colorbar(im,ax=ax,spacing='proportional')
#cb.set_ticks(list(range(vmax+1))+[vCenter])
#fig.colorbar(im,ax=ax)
#plt.clim((0,35))
ax.set_xticklabels({})
ax.set_yticklabels({})
if save == True:
    plt.savefig('Figures/simu_vs_simu_standardized_all_metrics_with_mean_%s.png' % cases)
plt.close()



fig,ax = plt.subplots(figsize=(12,11))  
if div_norm:
    im = ax.imshow(grid - min_val_at_same_folder,origin='lower',norm=divnorm)
else:
    im = ax.imshow(grid - min_val_at_same_folder,origin='lower',vmin = vmin, vmax=vmax)
cb = fig.colorbar(im,ax=ax,spacing='proportional')
#cb.set_ticks(list(range(vmax+1))+[vCenter])
#fig.colorbar(im,ax=ax)
#plt.clim((0,35))
ax.set_xticklabels({})
ax.set_yticklabels({})
if save == True:
    plt.savefig('Figures/simu_vs_simu_standardized_all_metrics_%s.png' % cases)
plt.close()



#folder = 0
plt.rcParams.update({'font.size': 24})
N_replicates = 5
for folder in folders_to_show:
    score_dict = {}
    for folder2 in folders_to_show:
        scores = []
        for i in range(N_replicates,2*N_replicates):
            indices = np.where(all_frames[folder][i]['Folder'] == folder2)[0]
            scores += list(all_frames[folder][i]['overall_deviation_score'][indices])
        score_dict[folder2] = copy(scores)
    fig,ax = plt.subplots(figsize=(10,10))
    ax.boxplot(score_dict.values())
    plt.savefig('Figures/box_plot_simu_cmp_from_folder_%05d' % folder)
    plt.close()

    with open('Figures/box_plot_simu_cmp_from_folder_%05d_p_values.txt' % folder, 'w') as f:
        for val in score_dict.values():
            #print(stats.ttest_ind(val,score_dict[folder],equal_var=False))
            p_value = stats.ttest_ind(val,score_dict[folder],equal_var=False).pvalue
            star_coding = ''
            if p_value > 0.05:
                star_coding = 'ns'
            elif p_value > 0.01:
                star_coding = '*'
            elif p_value > 0.001:
                star_coding = '**' 
            elif p_value > 0.0001:
                star_coding = '***'
            else:
                star_coding = '****'                                       
            f.write('%1.8f\t(%s)\n' % (p_value,star_coding))



#Expe vs expe




for day in [1,2,3]:
    with open('metric_comparison/expe_vs expe_data_day_%d_17_11_22.p' % day, 'rb') as f:
        all_frames = pickle.load(f)

    N_replicates = 3

    N = len(all_frames)*N_replicates
    collagen_list = [1,2,3,4]
    grid = np.zeros((N,N),dtype=float)
    for i in range(N):
        for j in range(N):
            row_folder = collagen_list[i // N_replicates]
            row_replicate = i % N_replicates + 1
            col_folder = collagen_list[j // N_replicates]
            col_replicate = j % N_replicates + 1 
            frame = all_frames[row_folder][row_replicate]
            index = np.where((frame['Folder'] == col_folder)  & (frame['Replicate'] == col_replicate))[0]
            if index.size > 0:
                grid[i,j] = frame['overall_deviation_score'][index[0]]
            else:
                print('%d, %d, %d, %d' % (row_folder, row_replicate, col_folder, col_replicate))

    grid_with_mean = grid.copy()
    for i in range(len(collagen_list)):
        for j in range(i+1):
            mean_val = np.mean(grid[(i*N_replicates):(i+1)*N_replicates,(j*N_replicates):(j+1)*N_replicates].flatten())
            if i != j:
                grid_with_mean[(i*N_replicates):(i+1)*N_replicates,(j*N_replicates):(j+1)*N_replicates] = mean_val
            else:
                for k in range(N_replicates):
                    grid_with_mean[i*N_replicates+k:(i+1)*N_replicates,j*N_replicates + k] = mean_val

    for i in range(grid.shape[0]):
        grid_with_mean[i,i] = grid[i,i]
                #grid_with_mean[(i*N_replicates):(i+1)*N_replicates,(j*N_replicates):(j+1)*N_replicates] = mean_val


    mean_grid = np.zeros((len(collagen_list),len(collagen_list)),dtype=float)
    for i in range(len(collagen_list)):
        for j in range(len(collagen_list)):
            mean_grid[i,j] = np.mean( grid[i*N_replicates : (i+1)*N_replicates, j*N_expe_replicates : (j+1) * N_expe_replicates].flatten() )

    fig,ax = plt.subplots(figsize=(12,11))  
    if div_norm:
        im = ax.imshow(grid_with_mean - min_val_at_same_folder,origin='lower', norm=divnorm)
    else:
        im = ax.imshow(grid_with_mean - min_val_at_same_folder,origin='lower', vmin = vmin, vmax=vmax)#,norm=divnorm,aspect='auto')
    cb = fig.colorbar(im,ax=ax,spacing='proportional')
    #cb.set_ticks(list(range(vmax+1))+[vCenter])
    #fig.colorbar(im,ax=ax)
    #plt.clim((0,35))
    ax.set_xticklabels({})
    ax.set_yticklabels({})
    if save == True:
        plt.savefig('Figures/expe_vs_expe_day_%d_standardized_all_metrics_with_mean.png' % day)
    plt.close()



    fig,ax = plt.subplots(figsize=(12,11))
    if div_norm:
        im = ax.imshow(mean_grid - min_val_at_same_folder,origin='lower',norm=divnorm)
    else:
        im = ax.imshow(mean_grid - min_val_at_same_folder,origin='lower',vmin = vmin, vmax=vmax)#,norm=divnorm,aspect='auto')
    cb = fig.colorbar(im,ax=ax,spacing='proportional')
    #cb.set_ticks(list(range(vmax+1))+[vCenter])
    #fig.colorbar(im,ax=ax)
    #plt.clim((0,35))
    ax.set_xticklabels({})
    ax.set_yticklabels({})
    if save == True:
        plt.savefig('Figures/expe_vs_expe_day_%d_standardized_all_metrics_mean.png' % day)
    plt.close()



    plt.rcParams.update({'font.size': 32})

    N_replicates = 3
    min_dev_score_val = 0
    for folder in [1,2,3,4]:
        score_dict = {}
        for folder2 in [1,2,3,4]:
            scores = []
            for i in range(1,N_replicates+1):
                indices = np.where(all_frames[folder][i]['Folder'] == folder2)[0]
                scores += list(all_frames[folder][i]['overall_deviation_score'][indices] + np.abs(min_dev_score_val))
            score_dict[folder2] = copy(scores)

        fig,ax = plt.subplots(figsize=(10,10))
        ax.boxplot(score_dict.values())
        ax.set_ylim((0,5))
        ax.set_xticks([])
        plt.savefig('Figures/box_plot_expe_cmp_day_%d_from_folder_%05d' % (day,folder))
        plt.close()
        with open('Figures/box_plot_expe_cmp_day_%d_from_folder_%05d_p_values.txt' % (day,folder), 'w') as f:
            for val in score_dict.values():
                #print(stats.ttest_ind(val,score_dict[folder],equal_var=False))
                p_value = stats.ttest_ind(val,score_dict[folder],equal_var=False).pvalue
                star_coding = ''
                if p_value > 0.05:
                    star_coding = 'ns'
                elif p_value > 0.01:
                    star_coding = '*'
                elif p_value > 0.001:
                    star_coding = '**' 
                elif p_value > 0.0001:
                    star_coding = '***'
                else:
                    star_coding = '****'                                       
                f.write('%1.8f\t(%s)\n' % (p_value,star_coding))
        